import argparse
import json
import logging
import math
import typing

from owslib.wfs import WebFeatureService
from owslib.csw import CatalogueServiceWeb
from owslib.fes import PropertyIsEqualTo, PropertyIsLike


logger = logging.getLogger("console")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s - %(process)d - %(processName)s - %(name)s - %(levelname)s - %(message)s"
)
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(formatter)
logger.addHandler(consoleHandler)


def _connect_to_wfs_2(url: str) -> WebFeatureService:
    wfs = WebFeatureService(url=url, version="2.0.0", verify=False)
    logger.debug(wfs)
    return wfs


def _list_wfs_feature_types(wfs: WebFeatureService) -> typing.List[str]:
    feature_types = wfs.contents
    logger.debug(feature_types)
    return feature_types


def _connect_to_csw(url: str) -> CatalogueServiceWeb:
    csw = CatalogueServiceWeb(url)
    return csw


def count_download_service_type_metadata(csw_object) -> None:
    dow_service_filter = PropertyIsEqualTo("serviceType", "download")
    try:
        csw_object.getrecords2(
            constraints=[dow_service_filter],
            outputschema="http://www.isotc211.org/2005/gmd",
            esn="hits",
            typenames="gmd:MD_Metadata",
        )
    except:
        logger.debug(csw_object.request)
        logger.debug(csw_object.response)
        raise
    else:
        csw_total = csw_object.results.get("matches")
        pages = csw_total / 50
        logger.info(csw_object.request)
        logger.info(csw_object.results)
        return csw_total, pages, dow_service_filter


def collect_csw_download_service_metadata_identifiers(csw_object, records=50):
    csw_metadata_ids = []
    matches, pages, ogc_filter = count_download_service_type_metadata(csw_object)
    page = 0
    while page <= math.ceil(pages - 1):
        start = (page * records) + 1
        try:
            csw_object.getrecords2(
                constraints=[ogc_filter],
                outputschema="http://www.isotc211.org/2005/gmd",
                typenames="gmd:MD_Metadata",
                esn="brief",
                startposition=start,
                maxrecords=records,
            )
        except AttributeError as e:
            logger.debug(e)
        else:
            csw_metadata_ids.append(list(csw_object.records))
        page += 1
    return csw_metadata_ids


def _parse_online_resource(record_distribution) -> dict:
    resources = []
    try:
        online_resources = record_distribution.distribution.online
    except AttributeError as e:
        logger.exception(
            f"Exception {e} for metadata record id '{record_distribution.identifier}'"
        )
        return None
    for res in online_resources:
        logger.debug(res)
        resources.append(
            {
                "url": res.url,
                "protocol": res.protocol,
                "functions": res.function,
                "name": res.name,
                "description": res.description,
            }
        )
    return resources


def collect_csw_metadata_record_by_id_service_urls(csw_object, file_identifier):
    try:
        csw_object.getrecordbyid(
            [file_identifier],
            outputschema="http://www.isotc211.org/2005/gmd",
            esn="full",
        )
    except:
        logger.exception(csw_object.response)
        raise
    else:
        for record in csw_object.records:
            logger.debug(record)
            md_resource = {
                "id": record,
                "online_resources": _parse_online_resource(csw_object.records[record]),
            }
        return md_resource


def collect_csw_metadata_download_service_urls(csw_object, records=50):
    page = 0
    matches, pages, ogc_filter = count_download_service_type_metadata(csw_object)
    online_resources = []
    while page <= math.ceil(pages - 1):
        start = (page * records) + 1
        try:
            csw_object.getrecords2(
                constraints=[ogc_filter],
                outputschema="http://www.isotc211.org/2005/gmd",
                typenames="gmd:MD_Metadata",
                esn="full",
                startposition=start,
                maxrecords=records,
            )
        except Exception as e:
            raise
        else:
            for record in csw_object.records:
                logger.debug(record)
                md_resource = {
                    "id": record,
                    "online_resources": _parse_online_resource(
                        csw_object.records[record]
                    ),
                }
                online_resources.append(md_resource)
        page += 1
    return online_resources


def _save_to_json_file(fname: str, payload: dict) -> None:
    with open(fname, "w") as data_file:
        data_file.write(json.dumps(payload, indent=4))


def parse_cli():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--country_code",
        dest="country",
        help="Set the country code to harvest the metadata from, e.g. SK, AT, IT.",
        default='SK'
    )
    args = parser.parse_args()
    return args


def main():
    args = parse_cli()
    csw_url = f"https://inspire-geoportal.ec.europa.eu/GeoportalProxyWebServices/resources/OGCCSW202/{args.country.upper()}"
    csw_obj = _connect_to_csw(csw_url)
    ids = [
        item
        for sublist in collect_csw_download_service_metadata_identifiers(csw_obj)
        for item in sublist
    ]
    logger.debug(len(ids))
    online_resources = []
    for id in ids:
        online_resources.append(
            collect_csw_metadata_record_by_id_service_urls(csw_obj, id)
        )
    _save_to_json_file(f"dowload_online_resources_{args.country.upper()}.json", online_resources)


if __name__ == "__main__":
    main()
