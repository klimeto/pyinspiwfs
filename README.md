# PYINSPWFS - Python library to harvest WFS services from INSPIRE Geoportal CSW endpoint(s)
## Installation
### Create and activate python3 virtual environment on a directory
```bash
$ cd /tmp
$ mkdir pyinspiwfs
$ cd pyinspiwfs
$ python3.7 -m venv venv
$ source venv/bin/activate
```

### Clone the repo to repo dir and install dependencies
```bash
$ git clone https://gitlab.com/klimentomas/pyinspiwfs.git repo
$ cd repo
$ pip3.7 install -r requirements.txt
```

### Harvest Download Service endpoint from CSW (SK virtual CSW is default)
```bash
$ python pyinspiwfs/api/harvest.py --country_code SI
```

### Check JSON report created in the app folder
$ cat dowload_online_resources_SI.json